import com.epam.View.myView;
import com.epam.model.CustomerTableEntity;
import com.epam.model.MenuEntity;
import com.epam.model.TakeOrderEntity;
import com.epam.model.WaiterEntity;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.Query;
import org.hibernate.cfg.Configuration;

public class Main {
    private static final SessionFactory ourSessionFactory;

    static {
        try {
            ourSessionFactory = new Configuration().configure().buildSessionFactory();

        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    public static void main(final String[] args) throws Exception {
        final Session session = getSession();
        try {
              new myView().show();
        } finally {
            session.close();
            System.exit(0);
        }
    }


}
