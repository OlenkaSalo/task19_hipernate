package com.epam.View;

import com.epam.model.CustomerTableEntity;
import com.epam.model.MenuEntity;
import com.epam.model.TakeOrderEntity;
import com.epam.model.WaiterEntity;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class myView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static SessionFactory ourSessionFactory;


    public myView() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("A", "   A - Read all table");
        menu.put("B", "   B - Read customer table for know dish name");
        menu.put("C", "   C - Insert new order for customer table");

        menu.put("Q", "   Q - exit");

        methodsMenu.put("A", this::ReadAllTable);
        methodsMenu.put("B", this::ReadCustomerTableFilter);
        methodsMenu.put("C", this::insertTakeOrder);


    }


    static {
        try {
            ourSessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession(); //return opened session
    }
    private void ReadAllTable() {
        final Session session = getSession();
        Query query = session.createQuery("from " + "MenuEntity");
        System.out.format("\nTable Menu --------------------\n");
        System.out.format("%-3s %-12s %-12s  %s\n", "dish_name", "type_in_menu", "dish_amount", "price");
        for (Object obj : query.list()) {
            MenuEntity menu = (MenuEntity) obj;
            System.out.format("%-20s %-25s %-2s  %.5f\n", menu.getDishName(),
                    menu.getTypeInMenu(), menu.getDishAmount(), menu.getPrice());
        }

        query = session.createQuery("from " + "CustomerTableEntity");
        System.out.format("\nTable Customer_and_table --------------------\n");

        for (Object obj : query.list()) {
            CustomerTableEntity tableEntity = (CustomerTableEntity) obj;
            System.out.format("%s\n", tableEntity.getIdTable());
        }

        query = session.createQuery("from " + "WaiterEntity  ");
        System.out.format("\nTable Waiter --------------------\n");
        System.out.format("%-10s %-12s   %s\n", "id_waiter", "name", "lastname");
        for (Object obj : query.list()) {
            WaiterEntity waiter = (WaiterEntity) obj;
            System.out.format("%3s %-12s   %s\n", waiter.getIdWaiter(), waiter.getName(), waiter.getLastname());
        }

        query = session.createQuery("from " + "TakeOrderEntity ");
        System.out.format("\nTable Take_order --------------------\n");
        System.out.format("%5s %-25s %-7s %-15s %-4s  %s\n", "id_take_order", "dish_name", "order_amount", "ordertime", "bill", "id_table");
        for (Object obj : query.list()) {
            TakeOrderEntity order = (TakeOrderEntity) obj;
            System.out.format("%5d %-25s %7d %-15tR %.2f %s \n", order.getIdTakeOrder(), order.getMenuByDishName().getDishName(), order.getOrderAmount(), order.getOrdertime(), order.getBill(), order.getTableById().getIdTable());
        }
    }

    private void ReadCustomerTableFilter() {
        final Session session = getSession();
        Scanner input = new Scanner(System.in);
        System.out.println("Input number table for Dish_name: ");
        String table_in = input.next();

        CustomerTableEntity tableEntity = (CustomerTableEntity) session.load(CustomerTableEntity.class, table_in);
        if (tableEntity != null) {
            System.out.format("\n%s: %s\n", table_in, "Dish_name");
            for (TakeOrderEntity obj : tableEntity.getTakeOrdersByIdTable())
                System.out.format("    %s\n", obj.getMenuByDishName().getDishName());
        } else System.out.println("invalid number of table");
    }

    private  void insertTakeOrder() {
        final  Session session = getSession();
        try{
        Scanner input = new Scanner(System.in);
        System.out.println("Input new dish for table: ");
        String dish_name = input.nextLine();
        System.out.println("Input order amount: ");
        Integer orderAmount = input.nextInt();
        System.out.println("Input order Time: ");
        Integer timeInt  = input.nextInt();
        Time time = new Time(timeInt);
        System.out.println("Input bill: ");
        BigDecimal price = input.nextBigDecimal();
        System.out.println("Input number of table: ");
        String table = input.next();

        session.beginTransaction();
        TakeOrderEntity orderEntity = new TakeOrderEntity(dish_name,orderAmount,time,price,table);
        session.save(orderEntity);
        session.getTransaction().commit();
        System.out.println("end insert order");
        } finally {
            session.close();
            System.exit(0);
        }
    }
        private void outputMenu() {
            System.out.println("\nMENU:");
            for (String key : menu.keySet())
                if (key.length() == 1) System.out.println(menu.get(key));
        }

        private void outputSubMenu(String fig) {

            System.out.println("\nSubMENU:");
            for (String key : menu.keySet())
                if (key.length() != 1 && key.substring(0, 1).equals(fig)) System.out.println(menu.get(key));
        }

        public void show( ) {
            String keyMenu;
            do {
                outputMenu();
                System.out.println("Please, select menu point.");
                keyMenu = input.nextLine().toUpperCase();

                if (keyMenu.matches("^\\d")) {
                    outputSubMenu(keyMenu);
                    System.out.println("Please, select menu point.");
                    keyMenu = input.nextLine().toUpperCase();
                }

                try {
                    methodsMenu.get(keyMenu).print();
                } catch (Exception e) {
                }
            } while (!keyMenu.equals("Q"));
        }



    }

