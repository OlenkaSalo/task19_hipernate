package com.epam.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "waiter", schema = "new_restaurant")
public class WaiterEntity {
    @Id
    @Column(name = "id_waiter", nullable = false, length = 25)
    private String idWaiter;

    @Column(name = "name", nullable = true, length = 20)
    private String name;

    @Column(name = "lastname", nullable = true, length = 20)
    private String lastname;

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "waiter_and_table",  schema = "new_restaurant", joinColumns = @JoinColumn(name = "id_waiter", referencedColumnName = "id_waiter", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "id_table", referencedColumnName = "id_table", nullable = false))
    private Set<CustomerTableEntity> table;


    public String getIdWaiter() {
        return idWaiter;
    }

    public void setIdWaiter(String idWaiter) {
        this.idWaiter = idWaiter;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WaiterEntity that = (WaiterEntity) o;

        if (idWaiter != null ? !idWaiter.equals(that.idWaiter) : that.idWaiter != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (lastname != null ? !lastname.equals(that.lastname) : that.lastname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idWaiter != null ? idWaiter.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        return result;
    }


    public Set<CustomerTableEntity> getId_table(){return table;}

    public void setId_table(Set<CustomerTableEntity> table) {
        this.table = table;
    }
}
